// Write a program that prints the first 10 factorials

#include <stdio.h>

int main (void)
{
	int i,num;
	
	num = 1;
	i = 10;

	printf("Factorial Num     Total\n");

	do
	{
		num = num * i;\
		printf("%2i              %7i\n",i,num);
		i--;
	}
	while (i != 0);
	
	return 0;
}
