// Write a program to generate and display a table of n and n^2, for integer values
// of n ranging from 1 to 10. Be certain to print appropriate column headings.

#include <stdio.h>

int main (void)
{
	int n,n2;
	
	printf(" n   |   n^2\n -----------\n");
	
	for (int i = 1; i <= 10; i++)
	{
		n = i;
		n2 = i * i;
		printf("%2i   |   %3i\n", n, n2);
	}
	
	return 0;
	
}
