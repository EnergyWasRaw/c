/*  
					*This is a multiple part exercise*
	To round off an integer "i" to the next largest even multiple of another integer
"j", the following formula can be used:

			nextMultiple = i + j - i % j;

	For Example, to round off 256 days to the next largest number of days evenly divisible
by a week, values of "i = 256" and "j = 7" can be substituted into the preceding formula as
follows:

			nextMultiple	= 256 + 7 - 256 % 7;
							= 256 + 7 - 4
							= 259

	Write a program to find the next largest multiple for the following values of "i" and "j":
*/

#include <stdio.h>

int main (void)
{
	int var1 [3] = {365,12258,996};
	int var2 [3] = {7,23,4};

	int nextMultiple (int a, int b)
	{
		int result = (a + b) - (a % b);

		return result;
	}

	int i = 0;

	for (i = 0; i < 3; i++)
	{
		printf("%i\n", nextMultiple(var1[i],var2[i]));
	}
}