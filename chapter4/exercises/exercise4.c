// Write a program that converts 27 degrees Fahrenheit (F) to degrees Celsius (C)
// using the following formula: C = (F - 32) / 1.8

#include <stdio.h>

int main (void)
{
	float F = 27.0;
	float C = (F - 32) / 1.8;

	printf("27 degrees Fahrenheit is equal to %f degrees Celsius\n", C);

	return 0;
}