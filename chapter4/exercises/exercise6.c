// Write a program to evaluate the polynomial shown here:
// 3x^3 - 5x^2 + 6
// for x = 2.55

#include <stdio.h>
#include <math.h>

float main (void)
{
	float x = 2.55;

	float functPolynomial (float x)
	{
		float myExp1 = pwr(x,3.0);
		float myExp2 = pwr(x,2.0);
		
		float result = (3 * myExp1) - (5 * myExp2) + 6;

		printf ("The answer is: %f\n", result);

		return result;
	}

	functPolynomial(x);

	return 0;
}