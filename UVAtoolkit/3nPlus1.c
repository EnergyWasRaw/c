//Take input n, if n is odd, do 3n+1, if n is even, n/2
//If n==1, then STOP

#include <stdio.h>

int main (void)
{
	int num;
	printf("Please enter an integer: ");
	scanf("%i", &num);
	printf("%i ", num);
	
	while (num != 1)
	{
		if (num % 2 == 1)
		{
			num = 3 * num + 1;
		}
		else if (num % 2 == 0)
		{
			num = num / 2;
		}
		printf("%i ", num);
	}
	
	printf("\n");
	
	return 0;
}
