//2520 is the smallest number that can be divided by each of the numbers 
//from 1 to 10 without any remainder.

//What is the smallest positive number that is 
//evenly divisible by all of the numbers from 1 to 20?

#include <stdio.h>

int main (void)
{
	int n,i,tfMain,tfSub;
	
	tfMain = 0;
	n = 1;
	
	while (tfMain == 0)
	{
		tfSub = 1;
		
		for (i = 1; i < 21; i++)
		{
			if (n % i != 0)
			{
				tfSub = 0;
			}
		}
		
		if (tfSub == 1)
		{
			tfMain = 1;
		}
		
		else if (tfSub == 0)
		{
			n++;
		}
	}
	
	printf("\nThe smallest number divisible by all numbers between 1 and 20 is: %i\n", n);
	
}
