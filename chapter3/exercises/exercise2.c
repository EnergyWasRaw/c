#include <stdio.h>

//This exercise wanted me to print out the lines shown below in the printf statements.

int main (void)
{
	printf("In C, lowercase letters are significant.\n");
	printf("main is where program execution begins.\n");
	printf("Opening and closing braces enclose program statements in a routine.\n");
	printf("All program statements must be terminated by a semicolon.\n");

	return 0;
}